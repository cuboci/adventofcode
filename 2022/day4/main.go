package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	fn := fs.Arg(0)
	f := os.Stdin
	if fn != "-" {
		f, err = os.Open(fn)
		if err != nil {
			return fmt.Errorf("opening input file: %w", err)
		}
		defer f.Close()
	}

	var (
		sc        = bufio.NewScanner(f)
		contained int
		overlap   int
	)
	for sc.Scan() {
		s := strings.FieldsFunc(sc.Text(), func(r rune) bool { return r == ',' })
		s1, s2 := newSection(s[0]), newSection(s[1])
		if s1.contains(s2) || s2.contains(s1) {
			contained++
		}
		if s1.overlaps(s2) {
			overlap++
		}
	}
	if err = sc.Err(); err != nil {
		return fmt.Errorf("reading input: %w", err)
	}

	fmt.Println(contained)
	fmt.Println(overlap)

	return nil
}

type section struct {
	from, to int
}

func newSection(in string) section {
	f := strings.FieldsFunc(in, func(r rune) bool {
		return r == '-'
	})
	if len(f) != 2 {
		panic("invalid input")
	}
	from, err := strconv.Atoi(f[0])
	if err != nil {
		panic("invalid input")
	}
	to, err := strconv.Atoi(f[1])
	if err != nil {
		panic("invalid input")
	}
	return section{from: from, to: to}
}

func (s section) contains(s2 section) bool {
	return s2.from >= s.from && s2.to <= s.to
}

func (s section) overlaps(s2 section) bool {
	return s2.from <= s.to && s2.to >= s.from || s.from <= s2.to && s.to >= s2.from
}
