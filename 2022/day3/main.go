package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	fn := fs.Arg(0)
	f := os.Stdin
	if fn != "-" {
		f, err = os.Open(fn)
		if err != nil {
			return fmt.Errorf("opening input file")
		}
		defer f.Close()
	}

	var (
		sc    = bufio.NewScanner(f)
		elves []rucksack
	)
	for sc.Scan() {
		elves = append(elves, newRucksack(sc.Text()))
	}
	if err = sc.Err(); err != nil {
		return fmt.Errorf("reading input: %w", err)
	}

	part1(elves)
	part2(elves)

	return nil
}

type rucksack struct {
	contents    string
	left, right string
}

func newRucksack(contents string) rucksack {
	return rucksack{
		contents: contents,
		left:     contents[:len(contents)/2],
		right:    contents[len(contents)/2:],
	}
}

func (r rucksack) misplaced() byte {
	idx := strings.IndexAny(r.left, r.right)
	if idx < 0 {
		panic("invalid input")
	}
	return r.left[idx]
}

func part1(elves []rucksack) {
	var sum int
	for _, r := range elves {
		c := r.misplaced()
		if c >= 97 {
			c -= 96
		} else {
			c -= 38
		}
		sum += int(c)
	}
	fmt.Println(sum)
}

func part2(elves []rucksack) {
	var sum int
	for i := 0; i < len(elves)-2; i += 3 {
		e1, e2, e3 := elves[i], elves[i+1], elves[i+2]
		idx := strings.IndexFunc(e1.contents, func(r rune) bool {
			return strings.IndexRune(e2.contents, r) >= 0 &&
				strings.IndexRune(e3.contents, r) >= 0
		})
		c := e1.contents[idx]
		if c >= 97 {
			c -= 96
		} else {
			c -= 38
		}
		sum += int(c)
	}
	fmt.Println(sum)
}
