package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	f, err := os.Open(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	var (
		scorePart1 int
		scorePart2 int
		sc         = bufio.NewScanner(f)
	)
	for sc.Scan() {
		hands := strings.Fields(sc.Text())
		opp, me := signs[hands[0]], signs[hands[1]]
		outcome := outcomes[hands[1]]
		scorePart1 += part1(opp, me)
		scorePart2 += part2(opp, outcome)
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("reading input: %w", err)
	}

	fmt.Println(scorePart1)
	fmt.Println(scorePart2)

	return nil
}

type sign int

const (
	invalid sign = iota
	rock
	paper
	scissors
)

var signs = map[string]sign{
	"A": rock,
	"B": paper,
	"C": scissors,
	"X": rock,
	"Y": paper,
	"Z": scissors,
}

type outcome int

const (
	lose outcome = 3 * iota
	draw
	win
)

var outcomes = map[string]outcome{
	"X": lose,
	"Y": draw,
	"Z": win,
}

type match struct {
	opponent, me sign
}

var matches = map[match]outcome{
	{rock, rock}:         draw,
	{rock, paper}:        win,
	{rock, scissors}:     lose,
	{paper, rock}:        lose,
	{paper, paper}:       draw,
	{paper, scissors}:    win,
	{scissors, rock}:     win,
	{scissors, paper}:    lose,
	{scissors, scissors}: draw,
}

type strategy struct {
	opponent sign
	outcome  outcome
}

var rules = map[strategy]sign{
	{rock, lose}:     scissors,
	{rock, draw}:     rock,
	{rock, win}:      paper,
	{paper, lose}:    rock,
	{paper, draw}:    paper,
	{paper, win}:     scissors,
	{scissors, lose}: paper,
	{scissors, draw}: scissors,
	{scissors, win}:  rock,
}

func part1(opp, me sign) int {
	return int(matches[match{opp, me}]) + int(me)
}

func part2(opp sign, outcome outcome) int {
	return int(rules[strategy{opp, outcome}]) + int(outcome)
}
