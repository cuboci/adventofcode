package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}

	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	f, err := os.Open(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	var (
		elves   []elf
		current elf
	)
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		line := sc.Text()
		if len(line) == 0 {
			elves = append(elves, current)
			current = nil
			continue
		}
		v, err := strconv.Atoi(line)
		if err != nil {
			return fmt.Errorf("invalid input: %w", err)
		}
		current = append(current, v)
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("reading input file: %w", err)
	}
	elves = append(elves, current)

	part1(elves)
	part2(elves)

	return nil
}

type elf []int

func (e elf) calories() int {
	var sum int
	for _, c := range e {
		sum += c
	}
	return sum
}

func part1(elves []elf) {
	var highest int
	for _, e := range elves {
		c := e.calories()
		if c > highest {
			highest = c
		}
	}

	fmt.Println(highest)
}

func part2(elves []elf) {
	sort.Slice(elves, func(i, j int) bool {
		return elves[i].calories() > elves[j].calories()
	})

	if len(elves) < 3 {
		panic("too few elves")
	}

	fmt.Println(elves[0].calories() + elves[1].calories() + elves[2].calories())
}
