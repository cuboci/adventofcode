package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	fn := fs.Arg(0)
	f := os.Stdin
	if fn != "-" {
		f, err = os.Open(fn)
		if err != nil {
			return fmt.Errorf("opening input file: %w", err)
		}
		defer f.Close()
	}

	sc := bufio.NewScanner(f)

	// parse stacks first
	var lines []string
	for sc.Scan() {
		line := sc.Text()
		if len(line) == 0 {
			// stack lines read
			break
		}
		lines = append(lines, line)
	}
	stacks := getStacks(lines)

	// parse moves
	var moves []move
	for sc.Scan() {
		moves = append(moves, parseMove(sc.Text()))
	}
	if err = sc.Err(); err != nil {
		return fmt.Errorf("reading input: %w", err)
	}

	part1(copyStacks(stacks), moves)
	part2(stacks, moves)

	return nil
}

type move struct {
	crates   int
	from, to int
}

var moveRx = regexp.MustCompile(`^move (\d+) from (\d+) to (\d+)$`)

func parseMove(in string) move {
	sub := moveRx.FindStringSubmatch(in)
	if len(sub) != 4 {
		// 4 = the whole match + the three subgroups
		panic("invalid input")
	}

	var m move
	// ignore the errors, because the regex wouldn't match
	m.crates, _ = strconv.Atoi(sub[1])
	m.from, _ = strconv.Atoi(sub[2])
	m.to, _ = strconv.Atoi(sub[3])

	return m
}

func part1(stacks []*stack, moves []move) {
	for _, m := range moves {
		for i := 0; i < m.crates; i++ {
			r, ok := stacks[m.from-1].pop()
			if !ok {
				panic("stack invalid")
			}
			stacks[m.to-1].push(r)
		}
	}
	for _, s := range stacks {
		r, _ := s.pop()
		fmt.Printf("%c", r)
	}
	fmt.Println()
}

func part2(stacks []*stack, moves []move) {
	for _, m := range moves {
		items, ok := stacks[m.from-1].popItems(m.crates)
		if !ok {
			panic("stack invalid")
		}
		stacks[m.to-1].pushItems(items)
	}
	for _, s := range stacks {
		r, _ := s.pop()
		fmt.Printf("%c", r)
	}
	fmt.Println()
}
