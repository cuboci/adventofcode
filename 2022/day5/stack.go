package main

import (
	"fmt"
	"strings"
)

type stack struct {
	data []rune
}

func (s *stack) String() string {
	sb := &strings.Builder{}
	for _, r := range s.data {
		fmt.Fprintf(sb, "[%v] ", r)
	}
	return sb.String()
}

func (s *stack) push(item rune) {
	s.data = append(s.data, item)
}

func (s *stack) pushItems(items []rune) {
	s.data = append(s.data, items...)
}

func (s *stack) pop() (rune, bool) {
	if len(s.data) == 0 {
		return -1, false
	}
	r := s.data[len(s.data)-1]
	s.data = s.data[:len(s.data)-1]
	return r, true
}

func (s *stack) popItems(n int) ([]rune, bool) {
	if len(s.data) < n {
		return nil, false
	}
	items := s.data[len(s.data)-n:]
	s.data = s.data[:len(s.data)-n]
	return items, true
}

func (s *stack) clone() *stack {
	data := make([]rune, len(s.data))
	copy(data, s.data)
	return &stack{data: data}
}

func getStacks(lines []string) []*stack {
	stackNums := lines[len(lines)-1]
	var stacks []*stack
	for _, n := range strings.Fields(stackNums) {
		s := &stack{}
		idx := strings.Index(stackNums, n)
		for i := len(lines) - 2; i >= 0; i-- {
			r := rune(lines[i][idx])
			if r == ' ' {
				continue
			}
			s.push(r)
		}
		stacks = append(stacks, s)
	}
	return stacks
}

func copyStacks(stacks []*stack) []*stack {
	var st []*stack
	for _, s := range stacks {
		st = append(st, s.clone())
	}
	return st
}
