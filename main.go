package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	fn := fs.Arg(0)
	f := os.Stdin
	if fn != "-" {
		f, err = os.Open(fn)
		if err != nil {
			return fmt.Errorf("opening input file")
		}
		defer f.Close()
	}

	return nil
}
