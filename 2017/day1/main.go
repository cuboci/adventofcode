package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return fmt.Errorf("parsing command line: %w", err)
	}
	if fs.NArg() < 1 {
		return errors.New("missing input file")
	}

	data, err := os.ReadFile(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("reading input file: %w", err)
	}
	input, err := convert(data)

	return part1(input)
}

func convert(data []byte) ([]int, error) {
	return nil, nil
}

func part1(input []int) error {
	for _, digit := range input {
		_ = digit
	}
	return nil
}
