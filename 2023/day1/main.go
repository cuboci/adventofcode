package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:], os.Stdin, os.Stdout, os.Stderr); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	f, dfn, err := open(fs.Arg(0), stdin)
	if err != nil {
		return fmt.Errorf("opening input: %w", err)
	}
	defer dfn()

	var (
		sc    = bufio.NewScanner(f)
		lines []string
	)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return err
	}

	fmt.Println("Part 1: ", sum(lines, false))
	fmt.Println("Part 2: ", sum(lines, true))

	return nil
}

func open(fn string, stdin io.Reader) (io.Reader, func(), error) {
	if fn == "-" {
		return stdin, func() {}, nil
	}
	f, err := os.Open(fn)
	if err != nil {
		return nil, func() {}, err
	}
	return f, func() { f.Close() }, nil
}

func sum(input []string, convert bool) int {
	var (
		sum, val int
		idx      int
	)
	for _, line := range input {
		if convert {
			line = replaceWords(line)
		}
		idx = strings.IndexAny(line, "123456789")
		if idx < 0 {
			panic("invalid input")
		}
		digits := append([]byte(nil), line[idx], line[strings.LastIndexAny(line, "123456789")])
		val, _ = strconv.Atoi(string(digits))
		sum += val
	}

	return sum
}

var words = map[string]string{
	"one":   "1",
	"two":   "2",
	"three": "3",
	"four":  "4",
	"five":  "5",
	"six":   "6",
	"seven": "7",
	"eight": "8",
	"nine":  "9",
}

func replaceWords(in string) string {
	var out string
	for pos := 0; pos < len(in); {
		var digit string
		for v := range words {
			if strings.HasPrefix(in[pos:], v) {
				digit = v
				break
			}
		}
		if digit == "" {
			out += in[pos : pos+1]
			pos++
			continue
		}

		var (
			overlap bool
			start   = pos + len(digit) - 1
		)
		switch digit {
		case "one", "three", "five", "nine":
			if strings.HasPrefix(in[start:], "eight") {
				overlap = true
			}
		case "two":
			if strings.HasPrefix(in[start:], "one") {
				overlap = true
			}
		case "seven":
			if strings.HasPrefix(in[start:], "nine") {
				overlap = true
			}
		case "eight":
			if strings.HasPrefix(in[start:], "three") || strings.HasPrefix(in[start:], "two") {
				overlap = true
			}
		}
		pos += len(digit)
		if overlap {
			pos--
		}
		out += words[digit]
	}

	return out
}
