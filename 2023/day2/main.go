package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:], os.Stdin, os.Stdout, os.Stderr); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	f, dfn, err := open(fs.Arg(0), stdin)
	if err != nil {
		return fmt.Errorf("opening input: %w", err)
	}
	defer dfn()

	var (
		sc    = bufio.NewScanner(f)
		lines []string
	)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return err
	}

	games := parseGames(lines)
	fmt.Println("Part 1:", part1(games))
	fmt.Println("Part 2:", part2(games))

	return nil
}

func open(fn string, stdin io.Reader) (io.Reader, func(), error) {
	if fn == "-" {
		return stdin, func() {}, nil
	}
	f, err := os.Open(fn)
	if err != nil {
		return nil, func() {}, err
	}
	return f, func() { f.Close() }, nil
}

type cubeList map[string]int

var limits = cubeList{
	"red":   12,
	"green": 13,
	"blue":  14,
}

func (c cubeList) valid(cl cubeList) bool {
	for colour, count := range cl {
		if limits[colour] < count {
			return false
		}
	}

	return true
}

func parseGames(lines []string) []game {
	var ret []game
	for _, line := range lines {
		ret = append(ret, parseGame(line))
	}

	return ret
}

func part1(games []game) int {
	var sum int
	for _, g := range games {
		if limits.valid(g.maxCubes) {
			sum += g.id
		}
	}
	return sum
}

func part2(games []game) int {
	var sum int
	for _, g := range games {
		sum += g.power()
	}
	return sum
}

type game struct {
	id       int
	maxCubes cubeList
}

func parseGame(input string) game {
	ret := game{maxCubes: make(cubeList)}

	p := strings.Split(input, ":")
	ret.id, _ = strconv.Atoi(p[0][5:])

	for _, draw := range strings.Split(strings.TrimSpace(p[1]), ";") {
		for _, cubes := range strings.Split(strings.TrimSpace(draw), ",") {
			c := strings.Fields(strings.TrimSpace(cubes))
			colour := c[1]
			count, _ := strconv.Atoi(c[0])
			if count > ret.maxCubes[colour] {
				ret.maxCubes[colour] = count
			}
		}
	}

	return ret
}

func (g game) power() int {
	ret := 1
	for _, count := range g.maxCubes {
		ret *= count
	}
	return ret
}
