package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input file")
	}

	f, err := os.Open(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	var addresses []address
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		addresses = append(addresses, parseAddress(sc.Text()))
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("reading input file: %w", err)
	}

	var tls, ssl int
	for _, a := range addresses {
		if a.doesTLS() {
			tls++
		}
		if a.doesSSL() {
			ssl++
		}
	}
	fmt.Println(tls)
	fmt.Println(ssl)

	return nil
}

type address struct {
	raw       string
	blocks    []string
	hypernets []string
}

func parseAddress(raw string) address {
	var (
		addr   = address{raw: raw}
		offset int
		next   rune = '['
		token  string
	)
	for len(raw) > 0 {
		offset = strings.IndexRune(raw, next)
		if offset < 0 {
			offset = len(raw)
		}
		token = raw[:offset]
		if len(raw) == offset {
			raw = raw[offset:]
		} else {
			raw = raw[offset+1:]
		}
		if next == '[' {
			addr.blocks = append(addr.blocks, token)
			next = ']'
		} else {
			addr.hypernets = append(addr.hypernets, token)
			next = '['
		}
	}

	return addr
}

func (a address) doesTLS() bool {
	for _, n := range a.hypernets {
		if testABBA(n) {
			return false
		}
	}
	for _, n := range a.blocks {
		if testABBA(n) {
			return true
		}
	}
	return false
}

func (a address) doesSSL() bool {
	aba := make(map[string]struct{})
	for _, n := range a.blocks {
		for _, v := range getABA(n) {
			aba[v] = struct{}{}
		}
	}
	for v := range aba {
		outer, inner := v[0], v[1]
		v = fmt.Sprintf("%c%c%c", inner, outer, inner)
		for _, n := range a.hypernets {
			if strings.Index(n, v) >= 0 {
				return true
			}
		}
	}
	return false
}

func testABBA(in string) bool {
	for i := 0; i < len(in)-3; i++ {
		if in[i] == in[i+3] && in[i+1] == in[i+2] && in[i] != in[i+1] {
			return true
		}
	}
	return false
}

func getABA(in string) []string {
	var ret []string
	for i := 0; i < len(in)-2; i++ {
		if in[i] == in[i+2] && in[i+1] != in[i] {
			ret = append(ret, in[i:i+3])
		}
	}

	return ret
}
