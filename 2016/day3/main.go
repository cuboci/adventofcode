package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	flag.Parse()
	if flag.NArg() < 1 {
		return errors.New("missing input file")
	}
	f, err := os.Open(flag.Arg(0))
	if err != nil {
		return fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
	input := make([][]int, 3)
	for sc.Scan() {
		fields := strings.Fields(sc.Text())
		for i, f := range fields {
			v, err := strconv.Atoi(f)
			if err != nil {
				return fmt.Errorf("parsing input data: %w", err)
			}
			input[i] = append(input[i], v)
		}
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("reading input file: %w", err)
	}

	part1(input)
	part2(input)

	return nil
}

type data [][]int
type triangle []int

func part1(input data) {
	var triangles []triangle
	for i := 0; i < len(input[0]); i++ {
		t := triangle{input[0][i], input[1][i], input[2][i]}
		triangles = append(triangles, t)
	}
	countInvalid(triangles)
}

func part2(input data) {
	var triangles []triangle
	for i := 0; i < len(input[0]); i += 3 {
		t1 := triangle{input[0][i], input[0][i+1], input[0][i+2]}
		t2 := triangle{input[1][i], input[1][i+1], input[1][i+2]}
		t3 := triangle{input[2][i], input[2][i+1], input[2][i+2]}
		triangles = append(triangles, t1, t2, t3)
	}
	countInvalid(triangles)
}

func countInvalid(triangles []triangle) {
	var invalid int
	for _, t := range triangles {
		if t[0]+t[1] <= t[2] ||
			t[1]+t[2] <= t[0] ||
			t[2]+t[0] <= t[1] {
			invalid++
		}
	}
	fmt.Println(len(triangles) - invalid)
}
