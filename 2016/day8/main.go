package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}

	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	f, err := os.Open(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	var (
		sc  = bufio.NewScanner(f)
		ops []operation
	)
	for sc.Scan() {
		ops = append(ops, parseOperation(sc.Text()))
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("reading input file: %w", err)
	}

	d := newDisplay(6, 50)
	d.show(true)

	part1(d, ops)

	return nil
}

type opType int

const (
	rect opType = iota
	rotRow
	rotCol
)

type operation struct {
	op   opType
	a, b int
}

func parseOperation(in string) operation {
	f := strings.Fields(in)
	if len(f) != 2 && len(f) != 5 {
		panic("invalid input")
	}
	switch f[0] {
	case "rect":
		return newRect(f[1])
	case "rotate":
		switch f[1] {
		case "row":
			return newRotRow(f[2], f[4])
		case "column":
			return newRotCol(f[2], f[4])
		default:
			panic("invalid rotation type")
		}
	default:
		panic("invalid operation")
	}
}

var rectRx = regexp.MustCompile(`^(\d+)x(\d+)$`)

func newRect(dim string) operation {
	m := rectRx.FindStringSubmatch(dim)
	if len(m) != 3 {
		panic("invalid rectangle values")
	}

	rows, _ := strconv.Atoi(m[1]) // regex ensures digits-only values
	cols, _ := strconv.Atoi(m[2]) // regex ensures digits-only values

	return operation{op: rect, a: rows, b: cols}
}

func newRotRow(what, by string) operation {
	what = strings.TrimPrefix(what, "y=")
	row, err := strconv.Atoi(what)
	if err != nil {
		panic(fmt.Errorf("invalid row syntax: %w", err))
	}
	amount, err := strconv.Atoi(by)
	if err != nil {
		panic(fmt.Errorf("invalid rotation amount: %w", err))
	}
	return operation{op: rotRow, a: row, b: amount}
}

func newRotCol(what, by string) operation {
	what = strings.TrimPrefix(what, "x=")
	col, err := strconv.Atoi(what)
	if err != nil {
		panic(fmt.Errorf("invalid column syntax: %w", err))
	}
	amount, err := strconv.Atoi(by)
	if err != nil {
		panic(fmt.Errorf("invalid rotation amount: %w", err))
	}
	return operation{op: rotCol, a: col, b: amount}
}

type display struct {
	rows, cols int
	data       [][]rune
}

func newDisplay(rows, cols int) *display {
	d := &display{
		rows: rows,
		cols: cols,
		data: make([][]rune, rows),
	}
	for i := 0; i < rows; i++ {
		d.data[i] = []rune(strings.Repeat(".", cols))
	}
	return d
}

func (d *display) apply(op operation) {
	switch op.op {
	case rect:
		for row := 0; row < op.b; row++ {
			for col := 0; col < op.a; col++ {
				d.data[row][col] = '#'
			}
		}
	case rotRow:
		row := d.data[op.a]
		row = append(row[len(row)-op.b:], row[:len(row)-op.b]...)
		d.data[op.a] = row
	case rotCol:
		column := make([]rune, d.rows)
		for i := 0; i < d.rows; i++ {
			column[i] = d.data[i][op.a]
		}
		column = append(column[len(column)-op.b:], column[:len(column)-op.b]...)
		for i := 0; i < d.rows; i++ {
			d.data[i][op.a] = column[i]
		}
	}
}

func (d *display) show(move bool) {
	for row := 0; row < d.rows; row++ {
		for col := 0; col < d.cols; col++ {
			fmt.Printf("%c", d.data[row][col])
		}
		fmt.Println()
	}
	if move {
		os.Stdout.Write([]byte{0x1b, '[', '6', 'A'})
	}
}

func (d *display) lit() int {
	var ret int
	for _, r := range d.data {
		for _, c := range r {
			if c == '#' {
				ret++
			}
		}
	}
	return ret
}

func part1(d *display, ops []operation) {
	for _, o := range ops {
		d.apply(o)
		time.Sleep(20 * time.Millisecond)
		d.show(true)
	}
	d.show(false)
	fmt.Printf("pixels lit: %d\n", d.lit())
}
