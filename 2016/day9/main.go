package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}

	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	data, err := os.ReadFile(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("reading input file: %w", err)
	}
	input := strings.TrimSpace(string(data))

	output := decompress(input)
	fmt.Println(len(output))

	return nil
}

func decompress(input string) string {
	sb := &strings.Builder{}

	var offset int
	for {
		start := strings.IndexByte(input, '(')
		if start < 0 {
			sb.WriteString(input[offset:])
			break
		}
		end := strings.IndexByte(input, ')')
		_ = end
	}
	/*
		for i := 0; i < len(input); i++ {
			c := input[i]
			if c != '(' {
				sb.WriteByte(c)
				continue
			}
			end := strings.IndexByte(input[i:], ')')
			if end < 0 {
				panic(fmt.Errorf("missing marker end after position %d", i))
			}
			chars, repeat := marker(input[i+1 : end])

			out := decompress(input[end+1:])
			input = input[] + out

			fmt.Println(chars, repeat)
		}
	*/
	return sb.String()
}

func marker(in string) (int, int) {
	f := strings.Split(in, "x")
	if len(f) != 2 {
		panic("invalid marker")
	}
	chars, err := strconv.Atoi(f[0])
	if err != nil {
		panic(fmt.Errorf("invalid number of characters: %w", err))
	}
	repeat, err := strconv.Atoi(f[1])
	if err != nil {
		panic(fmt.Errorf("invalid repeat count: %w", err))
	}

	return chars, repeat
}
