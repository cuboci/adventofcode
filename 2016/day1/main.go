package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		log.Fatal("missing input file")
	}

	path, err := readInput(flag.Arg(0))
	if err != nil {
		log.Fatal(err)
	}

	part1(path)
	part2(path)
}

type direction string

const (
	dirLeft  = direction("L")
	dirRight = direction("R")
)

type instruction struct {
	dir  direction
	dist int
}

type input []instruction

func readInput(filename string) (input, error) {
	f, err := os.Open(flag.Arg(0))
	if err != nil {
		return nil, fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
	sc.Split(bufio.ScanWords)

	var (
		path input
		inst instruction
		v    string
	)
	for sc.Scan() {
		v = sc.Text()
		if i := strings.IndexRune(v, ','); i > 0 {
			v = v[:i]
		}
		_, err = fmt.Sscanf(v, "%1s%d", &inst.dir, &inst.dist)
		if err != nil {
			return nil, fmt.Errorf("reading instruction: %w", err)
		}
		path = append(path, inst)
	}
	if err := sc.Err(); err != nil {
		return nil, fmt.Errorf("reading input: %w", err)
	}

	return path, nil
}

type view int

const (
	north view = iota
	east
	south
	west
)

func turn(v view, d direction) view {
	if d == dirLeft {
		v--
	} else {
		v++
	}

	if v < 0 {
		v = 4 + v
	}
	if v > 3 {
		v = 4 - v
	}

	return v
}

func part1(path input) {
	var (
		dir = north
		pos point
	)
	for _, inst := range path {
		dir = turn(dir, inst.dir)
		switch dir {
		case north:
			pos.y += inst.dist
		case east:
			pos.x += inst.dist
		case south:
			pos.y -= inst.dist
		case west:
			pos.x -= inst.dist
		}
	}
	fmt.Printf("part 1: x: %d, y: %d\n", pos.x, pos.y)
}

type point struct {
	x, y int
}

func walk(pos point, dir view, dist int) []point {
	var points []point
	switch dir {
	case north:
		for i := 1; i <= dist; i++ {
			points = append(points, point{pos.x, pos.y + i})
		}
	case east:
		for i := 1; i <= dist; i++ {
			points = append(points, point{pos.x + i, pos.y})
		}
	case south:
		for i := 1; i <= dist; i++ {
			points = append(points, point{pos.x, pos.y - i})
		}
	case west:
		for i := 1; i <= dist; i++ {
			points = append(points, point{pos.x - i, pos.y})
		}
	}
	return points
}

func part2(path input) {
	var (
		visited = map[point]struct{}{{}: {}} // initialise with starting point
		dir     = north
		pos     point
	)

	for _, inst := range path {
		dir = turn(dir, inst.dir)
		points := walk(pos, dir, inst.dist)
		for _, p := range points {
			pos = p
			if _, ok := visited[p]; ok {
				fmt.Printf("part 2: x: %d, y: %d\n", p.x, p.y)
				return
			}
			visited[p] = struct{}{}
		}
	}
}
