package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input file")
	}

	f, err := os.Open(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	var columns []*strings.Builder
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		for i, c := range sc.Text() {
			if len(columns) < i+1 {
				columns = append(columns, &strings.Builder{})
			}
			columns[i].WriteRune(c)
		}
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("parsing input file: %w", err)
	}

	var input []string
	for _, c := range columns {
		input = append(input, c.String())
	}

	part1(input)
	part2(input)

	return nil
}

func part1(input []string) {
	for _, c := range input {
		counts := make(map[rune]int)
		for _, r := range c {
			counts[r]++
		}
		order := make([]rune, 0, len(counts))
		for k := range counts {
			order = append(order, k)
		}
		sort.Slice(order, func(i, j int) bool {
			c1 := order[i]
			c2 := order[j]
			if counts[c1] > counts[c2] {
				return true
			}
			return false
		})
		fmt.Printf("%c", order[0])
	}
	fmt.Println()
}

func part2(input []string) {
	for _, c := range input {
		counts := make(map[rune]int)
		for _, r := range c {
			counts[r]++
		}
		order := make([]rune, 0, len(counts))
		for k := range counts {
			order = append(order, k)
		}
		sort.Slice(order, func(i, j int) bool {
			c1 := order[i]
			c2 := order[j]
			if counts[c1] > counts[c2] {
				return true
			}
			return false
		})
		fmt.Printf("%c", order[len(order)-1])
	}
	fmt.Println()
}
