package main

import (
	"crypto/md5"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input")
	}

	roomId := fs.Arg(0)
	part1(roomId)
	part2(roomId)

	return nil
}

func part1(roomId string) {
	var (
		hash     = md5.New()
		i, found int
		sum      string
	)

	for found < 8 {
		hash.Reset()
		io.WriteString(hash, fmt.Sprintf("%s%d", roomId, i))
		sum = fmt.Sprintf("%02x", hash.Sum(nil))
		if strings.HasPrefix(sum, "00000") {
			fmt.Printf("%c", sum[5])
			found++
		}
		i++
	}
	fmt.Println()
}

func part2(roomId string) {
	var (
		hash          = md5.New()
		i, found, pos int
		sum           string
		pass          = []byte("        ")
	)
	for found < 8 {
		hash.Reset()
		io.WriteString(hash, fmt.Sprintf("%s%d", roomId, i))
		i++

		sum = fmt.Sprintf("%02x", hash.Sum(nil))
		if !strings.HasPrefix(sum, "00000") {
			continue
		}

		pos = int(sum[5]) - 48
		if pos < 0 || pos > 7 || pass[pos] != ' ' {
			continue
		}
		found++
		pass[pos] = sum[6]
		fmt.Printf("\r%s", string(pass))
	}
	fmt.Println()
}
