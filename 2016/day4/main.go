package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	fs := flag.NewFlagSet(prog, flag.ContinueOnError)
	err := fs.Parse(args)
	if errors.Is(err, flag.ErrHelp) {
		return nil
	}
	if err != nil {
		return err
	}
	if fs.NArg() < 1 {
		return errors.New("missing input file")
	}

	rooms, err := readRooms(fs.Arg(0))
	if err != nil {
		return fmt.Errorf("reading input data: %w", err)
	}

	rooms = removeDecoys(rooms)
	part1(rooms)
	part2(rooms)

	return nil
}

type room struct {
	name     string
	sector   int
	checksum string
}

func readRooms(inputFile string) ([]room, error) {
	rx := regexp.MustCompile(`^((?:[a-z]+-)+)([0-9]+)\[([a-z]{5})\]$`)
	f, err := os.Open(inputFile)
	if err != nil {
		return nil, fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	var (
		rooms []room
		r     room
	)
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		m := rx.FindStringSubmatch(sc.Text())
		r.name = strings.TrimSuffix(m[1], "-")
		r.sector, _ = strconv.Atoi(m[2]) // no error possible here, because the regex wouldn't match
		r.checksum = m[3]
		rooms = append(rooms, r)
	}
	if err := sc.Err(); err != nil {
		return nil, fmt.Errorf("parsing input data: %w", err)
	}

	return rooms, nil
}

func removeDecoys(rooms []room) []room {
	var real []room
	for _, r := range rooms {
		var (
			counts = make(map[rune]int)
			order  []rune
		)
		for _, c := range r.name {
			if c == '-' {
				continue
			}
			counts[c]++
		}
		for c := range counts {
			order = append(order, c)
		}
		sort.Slice(order, func(i, j int) bool {
			c1, c2 := order[i], order[j]
			if counts[c1] > counts[c2] {
				return true
			}
			if counts[c1] == counts[c2] {
				return c1 < c2
			}
			return false
		})
		order = order[:5]
		if string(order) == r.checksum {
			real = append(real, r)
		}
	}
	return real
}

func part1(rooms []room) {
	var sum int
	for _, r := range rooms {
		sum += r.sector
	}
	fmt.Println(sum)
}

func part2(rooms []room) {
	for _, r := range rooms {
		var name []rune
		for _, c := range r.name {
			if c != '-' {
				c = (c-97+rune(r.sector))%26 + 97
			}
			name = append(name, c)
		}
		if string(name) == "northpole-object-storage" {
			fmt.Println(r.sector)
			break
		}
	}
}
