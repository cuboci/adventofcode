package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	if err := run(os.Args[0], os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

func run(prog string, args []string) error {
	flag.Parse()
	if flag.NArg() != 1 {
		return errors.New("missing input file")
	}

	f, err := os.Open(flag.Arg(0))
	if err != nil {
		return fmt.Errorf("opening input file: %w", err)
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
	var input [][]direction
	for sc.Scan() {
		var line []direction
		for _, c := range sc.Text() {
			line = append(line, direction(c))
		}
		input = append(input, line)
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("parsing input: %w", err)
	}

	part1(input)
	part2(input)

	return nil
}

type direction rune

const (
	up    direction = 'U'
	down  direction = 'D'
	left  direction = 'L'
	right direction = 'R'
)

func part1(input [][]direction) {
	pos := 5
	for _, line := range input {
		for _, inst := range line {
			switch inst {
			case up:
				if pos > 3 {
					pos -= 3
				}
			case down:
				if pos < 7 {
					pos += 3
				}
			case left:
				if pos != 1 && pos != 4 && pos != 7 {
					pos--
				}
			case right:
				if pos%3 != 0 {
					pos++
				}
			}
		}
		fmt.Print(pos)
	}
	fmt.Println()
}

func part2(input [][]direction) {
	pos := 10
	keypad := []rune{0, 0, '1', 0, 0, 0, '2', '3', '4', 0, '5', '6', '7', '8', '9', 0, 'A', 'B', 'C', 0, 0, 0, 'D', 0, 0}
	for _, line := range input {
		for _, inst := range line {
			var newpos = pos
			switch inst {
			case up:
				newpos -= 5
			case down:
				newpos += 5
			case left:
				if pos%5 != 0 {
					newpos--
				}
			case right:
				if pos%5 != 4 {
					newpos++
				}
			}
			if newpos >= 0 && newpos <= len(keypad)-1 && keypad[newpos] != 0 {
				pos = newpos
			}
		}
		fmt.Printf("%c", keypad[pos])
	}
	fmt.Println()
}
